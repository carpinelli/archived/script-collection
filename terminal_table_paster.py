#! /usr/bin/env python3

"""Copy a table from a spreadsheet, and paste one cell at a time."""

import time
# import logger

import pyperclip
import pyautogui


# Add optional delay time (e.g '0.1' or '0.2' or '2')
# Add logger option with verbosity levels


def main():
    Timeout = 5

    print("Please copy the table to the clipboard...")
    input("Press enter when done: ")

    table = []
    rows = [row for row in filter(None, pyperclip.paste().split("\n"))]

    for row in rows:
        table.append(tuple(filter(None, row.split('\t'))))

    print("\nPlease click the first field you want to paste to...\n")
    for seconds in range(Timeout, 0, -1):
        print(f"Beginning paste operation in {seconds}...")
        time.sleep(1)

    print(f"Pasting {len(table)} rows with {len(table[0])} entries...")

    for row in table:
        for cell in row:
            pyperclip.copy(cell)

            pyautogui.keyDown("ctrl")
            pyautogui.keyDown("shift")
            pyautogui.press('v')
            time.sleep(0.1)
            pyautogui.keyUp("ctrl")
            pyautogui.keyUp("shift")

            pyautogui.press("enter")
            time.sleep(0.1)

    pyautogui.press("enter")

    print("Copy complete. Exiting...")

    return


if __name__ == "__main__":
    main()
